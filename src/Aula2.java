import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Aula2 {
	
	public static void main(String[] args) {
		Aluno aluno = new Aluno("Teste", 123465, null);
		// Primeira parte explicação do "único" material na classe aluno
		/* Exemplo: forma de obter o material explicando a visibilidade */
//		System.out.println(aluno.material.getNome());
//		System.out.println(aluno.getMaterial().getNome());
//		
//		System.out.println("Nome: " + aluno.getMaterial().getNome());
//		System.out.println("Cor: " + aluno.getMaterial().getCor());
//		System.out.println("Tamanho: " + aluno.getMaterial().getTamanho());
		
		Material m1 = new Material("Caneta", "Preta", "Pequena");
		Material m2 = new Material("Borracha", "Branca", "Pequena");
		Material m3 = new Material("Apontador", "Preta", "Pequena");
		
		
		List<String> listaString = new ArrayList<String>(Arrays.asList("oi", "tchau", "boa tarde"));
		imprimirLista(listaString);
		
		List<Material> listaMaterial = new ArrayList<Material>(Arrays.asList(m1, m2, m3));	
		imprimirLista2(listaMaterial);
		
		
	}
	
	public static void imprimirLista(List<String> lista) {
		for (String batata : lista) {
			System.out.println(batata);
		}
	}
	
	// Explicação de List com Object
	public static void imprimirLista2(List<Material> lista) {
		for (Material batata : lista) {
			System.out.println(batata.getNome());
//			[Caneta, Borracha, Apontador, ...]
			
//			System.out.println(batata);
//			[Material@1, Material@2, Material@3, ...]
		}
	}

}
