import java.util.ArrayList;

public class Aula2Parte2 {
	
	public static void main(String[] args) {
		/* Exemplo de lista de objetos de outra classe e que classes parecidas podem ter o mesmo objeto */
		Aluno aluno = new Aluno("X", 123456, new ArrayList<Material>());
		Aluno aluno2 = new Aluno("Y", 123455, new ArrayList<Material>());
		

		Material m1 = new Material("Caneta", "Preta", "Pequena");
		Material m2 = new Material("Borracha", "Branca", "Pequena");
		Material m3 = new Material("Apontador", "Preta", "Pequena");
		
		aluno.getMaterial().add(m1);
		aluno.getMaterial().add(m2);
		
		aluno2.getMaterial().add(m1);
		aluno2.getMaterial().add(m2);		
		
		
		Aula2.imprimirLista2(aluno.getMaterial());
		System.out.println("");
		Aula2.imprimirLista2(aluno2.getMaterial());
	}

}
