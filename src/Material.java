
public class Material {
	
	private String nome;
	private String cor;
	private String tamanho;
	
	public Material(String nome, String cor, String tamanho) {
		super();
		this.nome = nome;
		this.cor = cor;
		this.tamanho = tamanho;
	}
	
	public Material() {
		// Vazio
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCor() {
		return cor;
	}
	public void setCor(String cor) {
		this.cor = cor;
	}
	public String getTamanho() {
		return tamanho;
	}
	public void setTamanho(String tamanho) {
		this.tamanho = tamanho;
	}
	
}
