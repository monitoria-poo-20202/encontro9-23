
public class Aula1 {

	public static void main(String[] args) {				
		/* COMO '"NÃO FAZER"' */
		Material caneta = new Material();
		caneta.setNome("Bic");
		caneta.setCor("Preta");
		caneta.setTamanho("Pequena");
		System.out.println("Nome: " + caneta.getNome());
		System.out.println("Cor: " + caneta.getCor());
		System.out.println("Tamanho: " + caneta.getTamanho());

		/* COMO FAZER */
		Material caneta2 = new Material("Bic", "Preta", "Pequena");
		System.out.println("\nNome: " + caneta2.getNome());
		System.out.println("Cor: " + caneta2.getCor());
		System.out.println("Tamanho: " + caneta2.getTamanho());
	}

}
