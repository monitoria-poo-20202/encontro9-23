import java.util.List;

public class Aluno {
	
	private String nome;
	private int matricula;
//	Material material;
	private List<Material> material;
	
	public Aluno(String nome, int matricula, List<Material> material) {
		super();
		this.nome = nome;
		this.matricula = matricula;
		this.material = material;
	}

	public int getMatricula() {
		return matricula;
	}

	public void setMatricula(int matricula) {
		this.matricula = matricula;
	}

	public List<Material> getMaterial() {
		return material;
	}

	public void setMaterial(List<Material> material) {
		this.material = material;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
		
	// Métodos para demonstrar o exemplo 1 da aula 2
//	public Material getMaterial() {
//		if(material == null) {
//			material = new Material("Caneta","Vermelha","Pequena");
//		}
//		return material;
//
//	}
	
	
	
	
	
	
	
}
